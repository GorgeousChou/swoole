<?php
// +----------------------------------------------------------------------
// 小黄牛blog - websocket - http发包给TCP
// +----------------------------------------------------------------------
// Copyright (c) 2018 https://xiuxian.junphp.com All rights reserved.
// +----------------------------------------------------------------------
// Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// Author: 小黄牛 <1731223728@qq.com>
// +----------------------------------------------------------------------
function https_request($url, $data = null){
	# 初始化一个cURL会话
	$curl = curl_init();  
	//设置请求选项, 包括具体的url
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);  //禁用后cURL将终止从服务端进行验证
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
	if (!empty($data)){
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);  //设置具体的post数据
	}
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);        
	$response = curl_exec($curl);  //执行一个cURL会话并且获取相关回复
	
$httpCode = curl_getinfo($curl,CURLINFO_HTTP_CODE); 
echo $httpCode;
	curl_close($curl);  //释放cURL句柄,关闭一个cURL会话
	return $response;
}
var_dump(https_request('http://IP:端口', [
	'user_id' => '用户ID',// 为空群发
	'content'=> '测试内容'
]));
