<?php
// +----------------------------------------------------------------------
// 小黄牛blog - websocket
// +----------------------------------------------------------------------
// Copyright (c) 2018 https://xiuxian.junphp.com All rights reserved.
// +----------------------------------------------------------------------
// Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// Author: 小黄牛 <1731223728@qq.com>
// +----------------------------------------------------------------------
session_start();
# 模拟用户登录
if (!empty($_POST['nice'])) {
	$data = [
		'nice' => $_POST['nice'],
		'id'   => uniqid(),
	];
	$_SESSION['user'] = $data;
	echo json_encode($data, JSON_UNESCAPED_UNICODE);
	exit;
# 模拟用于退出登录
} else if (!empty($_POST['out'])) {
	$_SESSION['user'] = '';
}
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Swoole+Websocket案例 - 小黄牛</title>
<style>
html,body{margin:0;padding:0;font-size:13px}
.left{width: 20%;height: 600px;border: 1px solid #ddd;float: left;}
.right{width: 59.7%;height: 400px;border: 1px solid #ddd;border-left: 0px;float: left;overflow: auto;}
.bottom{width: 79.7%;height: 199px;border: 1px solid #ddd;border-left: 0px;border-top: 0px;float: left;}
#content{width: 99.5%;height: 165px;}
.blue{color:blue}
.red{color:red}
.div_left{width:100%;float:left}
.div_right{width:100%;float:left;text-align: right;}
.div_centent{width:100%;float:left;text-align: center;}
#USER{width:100%;height: 40px;line-height: 40px;border-bottom: 1px solid #ddd;float:left}
#error{width:20%;height:400px;float: left;overflow: auto;}
</style>
<link rel="stylesheet" type="text/css" href="./css/user.css" />
<script src="./js/jquery.min.js"></script>
<script language="javascript" src="./js/jquery.easing.min.js"></script>
<script language="javascript" src="./js/custom.js"></script>
</head>

<body>
<!--登录导航-->
<div id="header">
    <div class="common">
        <div class="login fr">
            <ul>
				<li class="openlogin"><a href="" onclick="return false;">登录</a></li>
				<li class="reg" style="display:none"><a href="" onclick="return false;">退出</a></li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
</div>
<!--模拟登录弹窗-->
<div class="loginmask"></div>
<div id="loginalert">
    <div class="pd20 loginpd">
        <h3>
            <i class="closealert fr"></i>
            <div class="clear"></div>
        </h3>
        <div class="loginwrap">
            <div class="loginh">
                <div class="fl">模拟会员登录</div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
			<div class="logininput">
				<input type="text" class="loginusername" placeholder="随便输入一个昵称" />
			</div>
			<div class="clear"></div>
			<div class="loginbtn">
				<div class="loginsubmit fl">
					<input type="button" id="login_form" value="登录" />
					<div class="loginsubmiting">
						<div class="loginsubmiting_inner"></div>
					</div>
				</div>
			</div>
        </div>

        </div>
    </div>
</div>

<!--以下为聊天室窗口-->
<div id="USER"></div>
<div class="left">
	<ul>
		<li>用户列表：</li>
	</ul>
</div>
<div class="right"></div>
<div id="error"></div>
<div class="bottom">
	<textarea id="content"></textarea>
	<button type="button" id="submit">发送消息</button>
</div>


<h3>使用方法：</h3>
<p>①：CD进您的server.php文件目录</p>
<p>②：如果您是调试阶段，可以直接php server.php，激活程序，这样的话在运行过程中出错，能在cmd界面查看报错内容</p>
<p>③：如果您是部署阶段，可以使用nohup server.php >>/dev/null 2>&1 &命令，后台守护进程运行。</p>
</body>
</html>


<script>
var USER_ID = '';
var USER_NICE = '';
var lockReconnect = false; // 正常情况下我们是关闭心跳重连的
var wsServer = 'ws://127.0.0.1:9502';
var websocket;
var time;

<?php
if (isset($_SESSION['user'])) {
	$user = $_SESSION['user'];
	echo 'USER_ID = "'.$user['id'].'";';
	echo 'USER_NICE = "'.$user['nice'].'";';
	echo 'createWebSocket();';
	echo "$('.openlogin').hide();";
	echo "$('.login .reg').show();";
	echo "$('#USER').html('您的USER_ID为：".$user['id']." 昵称为：".$user['nice']."');";
}
?>

// ①开启WebSocket
function createWebSocket() {
	try {
		websocket = new WebSocket(wsServer);
		init();
	} catch(e) {
		reconnect(wsServer);
	}
}

// ②初始化WebSocket，并设置心跳检测
function init() {
	// 接收Socket断开时的消息通知
	websocket.onclose = function(evt) {
		$('#error').append('<p class="red">Socket断开了...正在试图重新连接...</p>');
		reconnect(wsServer);
	};
	// 接收Socket连接失败时的异常通知
	websocket.onerror = function(e){
		$('#error').append('<p class="red">Socket断开了...正在试图重新连接...</p>');
		reconnect(wsServer);
	};
	// 连接成功
	websocket.onopen = function (evt) {
		$('#error').append('<p class="blue">握手成功，打开socket连接了。。。</p>');
		var data = {
			'code':1, // 我们假设code为1时，是绑定登录请求
			'user_id':USER_ID,
			'user_nice':USER_NICE
		};
		// 前端发送json前，必须先转义成字符串
		data = JSON.stringify(data);
		console.log(data);
		websocket.send(data);
		// 心跳检测重置
		heartCheck.start();
	};

	var message = '';
	var flag    = true;
	// 接收服务端广播的消息通知
	websocket.onmessage = function(evt){
		heartCheck.start();
		var obj = JSON.parse(evt.data); 
		// 不存在，添加用户列表
		if ($("#"+obj.user_id).length>0) {}else if (obj.user_id != undefined && obj.user_id != null && obj.user_id != ''){
			$('.left ul').append('<li id="'+obj.user_id+'">'+obj.user_nice+' <span class="blue">(在线)</span></li>');
		}
		// 登录广播
		if (obj.code == 1) {
			// 存在修改上线状态
			if ($("#"+obj.user_id).length>0) {
				$("#"+obj.user_id+' span').removeClass('red');
				$("#"+obj.user_id+' span').addClass('blue');
				$("#"+obj.user_id+' span').html('在线');
			}
			$('.right').append('<div class="div_centent">'+obj.content+'</div>');
		// 下线广播 或 服务端强制下线广播
		} else if (obj.code == 2 || obj.code == 6) {
			// 存在修改下线状态
			if ($("#"+obj.user_id).length>0) {
				$("#"+obj.user_id+' span').removeClass('blue');
				$("#"+obj.user_id+' span').addClass('red');
				$("#"+obj.user_id+' span').html('离线');
				$('.right').append('<div class="div_centent">'+obj.content+'</div>');
			// 不存在，添加用户列表
			} else {
				$('.left ul').append('<li id="'+obj.user_id+'">'+obj.user_nice+' <span class="reg">(离线)</span></li>');
			}
		// 聊天消息广播
		} else if (obj.code == 3) {
			$('.right').append('<div class="div_left">'+obj.user_nice+'：'+obj.content+'</div>');
			// 聊天界面默认自动底部
			$('.right').scrollTop( $('.right')[0].scrollHeight );
		// 如果是心跳检测的广播就不做任何操作
		} else if (obj.code == 4){
			return false;
		// 检测是否后端发起了强制心跳检测，是则发送一次心跳检测
		} else if (obj.code == 5) {
			$('#error').append('<p class="red">服务端发起了一次强制心跳检测...</p>');
			var data = {
				'code':4, // 我们假设code为4时，既为心跳检测
				'user_id':USER_ID,
				'user_nice':USER_NICE
			};
			// 前端发送json前，必须先转义成字符串
			data = JSON.stringify(data);
			websocket.send(data);
		}
	};
}
// ③ 掉线重连
function reconnect(url) {
	if(lockReconnect) {
		return;
	};
	lockReconnect = true;
	// 没连接上会一直重连，设置心跳延迟避免请求过多
	time && clearTimeout(time);
	time = setTimeout(function () {
		createWebSocket(url);
		lockReconnect = false;
	}, 5000);
}
// ④心跳检测
var heartCheck = {
	timeout: 5000,
	timeoutObj: null,
	serverTimeoutObj: null,
	start: function() {
		var self = this;
		this.timeoutObj && clearTimeout(this.timeoutObj);
		this.serverTimeoutObj && clearTimeout(this.serverTimeoutObj);
		this.timeoutObj = setTimeout(function(){
			// 这里发送一个心跳，后端收到后，返回一个心跳消息，
			// onmessage拿到返回的心跳就说明连接正常
			var data = {
				'code':4, // 我们假设code为4时，既为心跳检测
				'user_id':USER_ID,
				'user_nice':USER_NICE
			};
			// 前端发送json前，必须先转义成字符串
			data = JSON.stringify(data);
			websocket.send(data);
		}, this.timeout)
	}
}
		
// 点击发送消息按钮
$('#submit').click(function(){
	if (USER_ID == '') {
		$('.openlogin').click();
		return;
	}
	var content = $('#content').val();
	$('.right').append('<div class="div_right">'+content+'：'+USER_NICE+'</div>');
	var data = {
		'code':3, // 我们假设code为3时，既为聊天消息广播请求
		'user_id':USER_ID,
		'user_nice':USER_NICE,
		'content':content
	};
	// 前端发送json前，必须先转义成字符串
	data = JSON.stringify(data);
	websocket.send(data);
	// 输入表单清空
	$('#content').val('');
	// 聊天界面默认自动底部
	$('.right').scrollTop( $('.right')[0].scrollHeight );
});

// 点击发送模拟登录请求
$('#login_form').click(function(){
	var nice    = $('.loginusername').val();

    $.ajax({
        type: 'post',
        data:{'nice':nice},
        url: "",
        success: function(data) {
            var obj = JSON.parse(data);
			$('.openlogin').hide();
			$('.login .reg').show();
			$('#USER').html('您的USER_ID为：'+obj.id+' 昵称为：'+obj.nice);
			$('.closealert').click();
			USER_ID = obj.id;
			USER_NICE = obj.nice;
			createWebSocket();
        }
    });

});

// 点击退出登录请求
$('.login .reg a').click(function(){
	$.ajax({
        type: 'post',
        data:{'out':1},
        url: "",
        success: function(data) {
            $('.openlogin').show();
			$('.login .reg').hide();
			$('#USER').html('');
			$("#"+USER_ID+' span').removeClass('blue');
			$("#"+USER_ID+' span').addClass('red');
			$("#"+USER_ID+' span').html('离线');
			USER_ID = '';
			USER_NICE = '';
			websocket.close();
        }
    });
});
</script>